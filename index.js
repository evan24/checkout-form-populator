function setFormValues(valueToSet, typeValue, elementType = 'input') {
    const searchString = `${elementType}${typeValue ? '[type=' + typeValue + ']': ''}`;
    const elements = document.querySelectorAll(searchString);
    console.log('Found ' + elements.length + ' for query ' + searchString + '. Processing, please wait as it will take a moment...');

    const reactEventHandlerKeyname = getEventHandlerKey(elements);
    elements.forEach((element) => {
        if (!element[reactEventHandlerKeyname]) {
            console.log(`key ${reactEventHandlerKeyname} does not exist on this input`);
            return;
        }

        if (!element[reactEventHandlerKeyname].onChange) {
            console.log(`key ${reactEventHandlerKeyname}[onChange] does not exist on this input`);
            return;
        }


        // This functionality short circuits the value that the user initially puts in.
        if (element.options) {
            element[reactEventHandlerKeyname].onChange({
                stopPropagation: 1,
                preventDefault: 1,
                target: {
                    type: 'notspecified',
                    value: element.options[1].value
                }
            })
            return;
        }

        if (element.name.includes('expected_time')) {
            element[reactEventHandlerKeyname].onChange({
                stopPropagation: 1,
                preventDefault: 1,
                target: {
                    type: 'notspecified',
                    value: '01:00:00'
                }
            })
            return;
        }
        
        if (element.name.includes('birthday') || element.name.includes('birthdate')) {
            element[reactEventHandlerKeyname].onChange({
                stopPropagation: 1,
                preventDefault: 1,
                target: {
                    type: 'notspecified',
                    value: '16.09.2019'
                }
            })
            return;
        }

        if (element.name.includes('noSpecificProductInformationNewsletter')) {
            return;
        }

        element[reactEventHandlerKeyname].onChange({
            stopPropagation: 1,
            preventDefault: 1,
            target: {
                type: 'notspecified',
                value: valueToSet
            }
        })
    })
}

function getEventHandlerKey(inputs) {
    let handlerKey = null;
    let currentInputIndex = 0;
    while (!handlerKey) {
        if (!inputs[currentInputIndex]) {
            return;
        }
        const keys = Object.keys(inputs[currentInputIndex]);
        handlerKey = keys.find((key) => key.includes('__reactEventHandler'));
        currentInputIndex++;
    }
    return handlerKey;
}


setFormValues('testData-script', 'text')
setFormValues('testData-script', undefined, 'textarea')
setFormValues('test@fitfox.de', 'email')
setFormValues('AB12345', 'mika_timing_chip_number')
setFormValues('yes', 'checkbox')
setFormValues('female', undefined, 'select')
console.log('------------------------------------------------------------------')
console.log('... done!');